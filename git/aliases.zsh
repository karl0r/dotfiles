#!/bin/sh
if which hub >/dev/null 2>&1; then
    alias git='hub'
fi

alias gl='git pull --prune'
alias gpu='git pull origin HEAD --prune'
alias glg="git log --graph --decorate --oneline --abbrev-commit"
alias glt='git log --all --graph --decorate --oneline --simplify-by-decoration' # pretty branch status
alias glp5='git log -5 --pretty --oneline' # view your last 5 latest commits each on their own line
alias glsw='git log -S' # search the commit history for the word puppy and display matching commits (glsw [word])
alias glga="glg --all"
alias gp='git push origin HEAD'
alias gpt="git push origin HEAD && git push --tags"
alias gpa='git push origin --all'
alias gd='git diff'
alias gc='git commit'
alias gm='git commit -m'
alias gam='git commit -am'
alias gca='git commit -a'
alias wip="git commit -m'WIP' . && git push origin HEAD"
alias gc='git checkout'
alias gra='git remote add'
alias grr='git remote rm'
alias gbt=git_list_branches
alias gb='git branch -v'
alias ga='git add'
alias gaa='git add -A'
alias gap='git add -p' # step through each change, or hunk
alias gcm='git commit -m'
alias gcam='git commit -a -m'
alias gs='git status -sb'
alias gsl='git shortlog -sn' # quickly get a list of contributors and see how many commits each person has
alias gws='git diff --shortstat "@{0 day ago}"' # how many lines of code you have written today
alias gwts='git ls-files | xargs wc -l' # count number of lines of code in a git project
alias ggmp='git checkout -' # jump back to to your last branch
alias gcl='git clone'
alias gta='git tag -a -m'
alias gcb='git-copy-branch-name'
alias gpr='gp && git pr'
alias gf='git reflog' # allows you to see every step you have made with git allowing you to retract and reinstate your steps
alias gst='git stash' # stash git changes and put them into your list
alias gdtp='git stash pop' # bring back your changes, but it removes them from your stash
alias gchp='git cherry-pick' # cherry pick the committed code in your own branch (gchp [hash])
alias gcln='git clean -xfd' # remove untracked files
alias undopush="git push -f origin HEAD^:master" # Undo a `git push`
alias gr='[ ! -z `git rev-parse --show-cdup` ] && cd `git rev-parse --show-cdup || pwd`' # `cd` to Git repo root

gi() {
	curl -s "https://www.gitignore.io/api/$*"
}
