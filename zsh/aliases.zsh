#!/bin/sh
alias reload!='exec "$SHELL" -l'

alias localrc="if [[ -a ~/.zsh.local ]]; then ${EDITOR} ~/.zsh.local; fi"
alias zshrc="${EDITOR} ${$(cd -P $(dirname $0) && cd ..; pwd -P)}"
alias reload="source ~/.zshrc && echo 'Shell config reloaded from ~/.zshrc'"
