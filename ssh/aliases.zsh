#!/bin/sh
alias pcipubkey="more ~/.ssh/id_rsa-pci.pub | pbcopy | echo '=> Public key copied to pasteboard.'"
